﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using PdfEdit.Drawing;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using QRCoder;
using static QRCoder.PayloadGenerator;
using System.Net;
//using PdfSharp.Drawing;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace documentos.Controllers
{
    [Route("api/PDF")]
    public class PDFController : Controller
    {
        [Route("[action]")]
        [HttpGet]
        public IActionResult Generar()
        {
            //string pdfpath = HttpServerUtility.MapPath("PDFs");
            //string imagepath = Server.MapPath("Columns");
            Document doc = new Document();

            try

            {
                byte[] documentoByte = null;

                var cssResolver =
                XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
                // HTML
                HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
                htmlContext.AutoBookmark(false);
                // Pipelines
                ElementList elements = new ElementList();
                ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
                HtmlPipeline html = new HtmlPipeline(htmlContext, end);
                CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
                // XML Worker
                XMLWorker worker = new XMLWorker(css, true);
                XMLParser p = new XMLParser(worker);

                using (MemoryStream ms = new MemoryStream())
                {
                    Document document = new Document(PageSize.LEGAL.Rotate());
                    // step 2
                    PdfWriter writer = PdfWriter.GetInstance(document, ms);
                    // step 3
                    document.Open();
                    // step 4
                    //Rectangle left = new Rectangle(36, 36, 486, 586);
                    iTextSharp.text.Rectangle right = new iTextSharp.text.Rectangle(522, 36, 972, 586);
                    ColumnText ct = new ColumnText(writer.DirectContent);
                    //ct.SetSimpleColumn(36, 36, 486, 586);
                    bool leftside = true;
                    //int status = ColumnText.START_COLUMN;
                    int linesWritten = 0;
                    int status = 0;
                    int column = 2;
                    //for (Element e : elements)
                    //{
                    //    if (ColumnText.isAllowedElement(e))
                    //    {
                    //        column.AddElement(e);
                    //        status = column.Go();
                    //        while (ColumnText.HasMoreText(status))
                    //        {
                    //            if (leftside)
                    //            {
                    //                leftside = false;
                    //                column.SetSimpleColumn(right);
                    //            }
                    //            else
                    //            {
                    //                document.newPage();
                    //                leftside = true;
                    //                column.setSimpleColumn(left);
                    //            }
                    //            status = column.go();
                    //        }
                    //    }
                    //}
                    //// step 5
                    ///
                    while (ColumnText.HasMoreText(status))
                    {
                        ct.SetSimpleColumn(36, 36, 486, 586);
                        ct.YLine = 486;
                        status = ct.Go();
                        linesWritten += ct.LinesWritten;
                        column = Math.Abs(column - 1);
                        if (column == 0) document.NewPage();
                    }

                    ct.AddText(new Phrase("Lines written: " + linesWritten));
                    ct.Go();
                    document.Close();
                    documentoByte = ms.ToArray();
                }


                //using (MemoryStream ms = new MemoryStream())
                //{
                //    PdfWriter.GetInstance(doc, ms);

                //    doc.Open();

                //    Paragraph heading = new Paragraph("Page Heading", new Font(Font.HELVETICA, 28f, Font.BOLD));

                //    heading.SpacingAfter = 18f;

                //    doc.Add(heading);

                //    string text = @"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse blandit blandit turpis. Nam in lectus ut dolor consectetuer bibendum. Morbi neque ipsum, laoreet id; dignissim et, viverra id, mauris. Nulla mauris elit, consectetuer sit amet, accumsan eget, congue ac, libero. Vivamus suscipit. Nunc dignissim consectetuer lectus. Fusce elit nisi; commodo non, facilisis quis, hendrerit eu, dolor? Suspendisse eleifend nisi ut magna. Phasellus id lectus! Vivamus laoreet enim et dolor. Integer arcu mauris, ultricies vel, porta quis, venenatis at, libero. Donec nibh est, adipiscing et, ullamcorper vitae, placerat at, diam. Integer ac turpis vel ligula rutrum auctor! Morbi egestas erat sit amet diam. Ut ut ipsum? Aliquam non sem. Nulla risus eros, mollis quis, blandit ut; luctus eget, urna. Vestibulum vestibulum dapibus erat. Proin egestas leo a metus?";

                //    iTextSharp.text.pdf.MultiColumnText columns = new iTextSharp.text.pdf.MultiColumnText();

                //    //float left, float right, float gutterwidth, int numcolumns

                //    columns.AddRegularColumns(36f, doc.PageSize.Width - 36f, 24f, 2);



                //    Paragraph para = new Paragraph(text, new Font(Font.HELVETICA, 8f));

                //    para.SpacingAfter = 9f;

                //    para.Alignment = Element.ALIGN_JUSTIFIED;

                //    //for (int i = 0; i < 8; i++)

                //    //{

                //    //    columns.AddElement(para);

                //    //}
                //    columns.AddText();


                //    doc.Add(columns);
                //    doc.Close();

                //    documentoByte = ms.ToArray();

                //}
                var ms1 = new MemoryStream();
                ms1.Write(documentoByte, 0, documentoByte.Length);
                ms1.Position = 0;
                return new FileStreamResult(ms1, "application/pdf");



            }

            catch (Exception ex)

            {

                //Log(ex.Message);

            }

            finally

            {



            }
            return Ok();
        }
        [Route("[action]")]
        [HttpGet]
        public ActionResult GenerarPDF2()
        {
            string PlantillaF= $@"<span></span><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.3333%;text-align:center;'><img src='https://virtual.bsginstitute.com/logo_pc2.png' alt='Logo' width='50' height='50' /></td><td style='width:33.333333333333336%;'>&nbsp;</td></tr></tbody></table><p style='text-align:center;'><span style='font-size:18px;'>&nbsp;</span></p><p style='text-align:center;'><span style='font-size:18px;'>CERTIFICADO</span></p><p>&nbsp;</p><p>Se otorga el siguiente certificado a:</p><p>&nbsp;</p><p style='text-align:center;'>FISCHER&nbsp;JUVENAL&nbsp;VALDEZ&nbsp;HERRERA</p><p style='text-align:center;'>&nbsp;</p><p>Por haber completado en forma satisfactoria el Programa Internacional:</p><p style='text-align:center;'>Curso Gestión de Proyectos</p><p style='text-align:center;'>Desarrollado desde el 3 de Abril del 2019 al 22 de Julio del 2019,&nbsp; </p><p style='text-align:center;'>con una duraci&oacute;n de 44 horas cronol&oacute;gicas,</p><p style='text-align:center;'>obteniendo una calificaci&oacute;n promedio de 37.09/100.00, </p><p style='text-align:center;'>en el cual se desarroll&oacute; el siguiente temario (detallado al reverso).</p><p>&nbsp;</p><p style='text-align:right;'>Lima, Enero del 2020</p><table><tbody><tr><td>&nbsp;</td><td></td></tr></tbody></table><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.333333333333336%;'><img src='http://integra.bsgrupo.net:900/Content/img/Coordinador/Certificado/certificado_firma_jimmy.png' alt='' width='300' height='120' /></td></tr></tbody></table><p style='text-align:right;'>9646038A123010620P0</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
            string PlantillaP="";
            byte[] documentoByte = null;


            using (MemoryStream ms = new MemoryStream())
            {
                FontFactory.RegisterDirectories();
                using (Document _document = new Document(PageSize.A4.Rotate(), 89, 89, 143, 75))
                {
                    FontFactory.GetFont("Times New Roman", 14, iTextSharp.text.BaseColor.BLACK);

                    HTMLWorker htmlparser = new HTMLWorker(_document);
                    htmlparser.SetStyleSheet(GenerateStyleSheet2());
                    PdfWriter.GetInstance(_document, ms);
                    _document.Open();
                    int Conteo = 0;
                    int cantidadItems = 0;
                    List<string> estructura = new List<string>();
                    if (PlantillaP != "" && PlantillaP != null)
                    {
                        estructura = PlantillaP.Split("id='estructura'").Where(x => x.StartsWith("><strong>")).ToList();
                        cantidadItems = estructura.Count();
                        var a = cantidadItems / 4;
                        Conteo = Convert.ToInt32(cantidadItems / 4);

                    }
                    //PdfPTable tableEstructura = new PdfPTable(Conteo);

                    //List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(PlantillaF), null);
                    //for (int k = 0; k < htmlarraylist.Count; k++)
                    //{
                    //    htmlarraylist[k].s
                    //    _document.Add((IElement)htmlarraylist[k]);
                    //}

                    htmlparser.Parse(new StringReader(PlantillaF));


                    _document.Close();
                }
                documentoByte = ms.ToArray();
            }
        

            var ms1 = new MemoryStream();
            ms1.Write(documentoByte, 0, documentoByte.Length);
            ms1.Position = 0;
            return new FileStreamResult(ms1, "application/pdf");
        }
        [Route("[action]")]
        [HttpGet]
        public ActionResult GenerarPDF3()
        {
            string PlantillaF= $@"<span></span><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.3333%;text-align:center;'><img src='https://virtual.bsginstitute.com/logo_pc2.png' alt='Logo' width='50' height='50' /></td><td style='width:33.333333333333336%;'>&nbsp;</td></tr></tbody></table><p style='text-align:center;'><span style='font-size:18px;'>&nbsp;</span></p><p style='text-align:center;'><span style='font-size:18px;'>CERTIFICADO</span></p><p>&nbsp;</p><p>Se otorga el siguiente certificado a:</p><p>&nbsp;</p><p style='text-align:center;'>FISCHER&nbsp;JUVENAL&nbsp;VALDEZ&nbsp;HERRERA</p><p style='text-align:center;'>&nbsp;</p><p>Por haber completado en forma satisfactoria el Programa Internacional:</p><p style='text-align:center;'>Curso Gestión de Proyectos</p><p style='text-align:center;'>Desarrollado desde el 3 de Abril del 2019 al 22 de Julio del 2019,&nbsp; </p><p style='text-align:center;'>con una duraci&oacute;n de 44 horas cronol&oacute;gicas,</p><p style='text-align:center;'>obteniendo una calificaci&oacute;n promedio de 37.09/100.00, </p><p style='text-align:center;'>en el cual se desarroll&oacute; el siguiente temario (detallado al reverso).</p><p>&nbsp;</p><p style='text-align:right;'>Lima, Enero del 2020</p><table><tbody><tr><td>&nbsp;</td><td></td></tr></tbody></table><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.333333333333336%;'><img src='http://integra.bsgrupo.net:900/Content/img/Coordinador/Certificado/certificado_firma_jimmy.png' alt='' width='300' height='120' /></td></tr></tbody></table><p style='text-align:right;'>9646038A123010620P0</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";
            string PlantillaP="";
            byte[] documentoByte = null;


            using (MemoryStream ms = new MemoryStream())
            {
                FontFactory.RegisterDirectories();
                using (Document _document = new Document(PageSize.A4.Rotate(), 89, 89, 143, 75))
                {
                    Paragraph comb = new Paragraph();
                    PdfPTable tableEstructura = new PdfPTable(1);
                    HTMLWorker htmlparser = new HTMLWorker(_document);
                    htmlparser.SetStyleSheet(GenerateStyleSheet2());
                    PdfWriter.GetInstance(_document, ms);
                    _document.Open();
                    StringBuilder sb = new StringBuilder();
                    sb.Append(PlantillaF);

                    ElementList list = XMLWorkerHelper.ParseToElementList(sb.ToString(), null);
                    foreach (IElement element in list)
                    {
                        comb.Add(element);
                    }

                    PdfPCell cell = new PdfPCell(new Paragraph(comb));
                    
                    cell.SetLeading(3f, 1.2f);

                    tableEstructura.AddCell(cell);
                    _document.Add(tableEstructura);                  


                    _document.Close();
                }
                documentoByte = ms.ToArray();
            }
        

            var ms1 = new MemoryStream();
            ms1.Write(documentoByte, 0, documentoByte.Length);
            ms1.Position = 0;
            return new FileStreamResult(ms1, "application/pdf");
        }
        
        [Route("[action]")]
        [HttpGet]
        public ActionResult GenerarPDF4()
        {
            string PlantillaF= $@"<span></span><p style='text-align:center;'>&nbsp;</p><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.3333%;text-align:center;'><img src='https://virtual.bsginstitute.com/logo_pc2.png' alt='Logo' width='50' height='50' style='text-align:center;' /></td><td style='width:33.333333333333336%;'>&nbsp;</td></tr></tbody></table><p style='text-align:center;'>&nbsp;</p><h1 style='text-align:center;'>&lt;dddddd&gt;<span style='font-family:'Times New Roman';'>&nbsp;</span><span style='font-family:'Times New Roman';'>CERTIFICADO</span><span style='font-family:'Times New Roman';'>&nbsp;</span>&lt;/dddddd&gt;<span style='font-family:'Times New Roman';text-align:center;'></span></h1><p style='text-align:left;'>&lt;dddddd&gt; <br />Se otorga el siguiente certificado a:</p><p style='text-align:center;'><span>FISCHER&nbsp;JUVENAL&nbsp;VALDEZ&nbsp;HERRERA</span></p><p style='text-align:center;'>&nbsp;</p><p style='text-align:left;'>Por haber completado en forma satisfactoria el Programa Internacional:</p><p style='text-align:center;'>Curso Gestión de Proyectos</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>Desarrollado desde el 05 de abril del 2019 al 24 de julio del 2019,&nbsp; con una duraci&oacute;n de 4 meses horas cronol&oacute;gicas,</p><p style='text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario <span style='font-size:7px;'>(detallado al reverso)</span>.</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:right;'>Lima, Mayo del 2020</p><p style='text-align:right;'>&lt;/dddddd&gt;&nbsp;</p><table><tbody><tr style='height:100%;'><td style='width:50%;'>&nbsp;</td><td style='width:50%;'><img src='http://integra.bsgrupo.net:900/Content/img/Coordinador/Certificado/certificado_firma_jimmy.png' alt='' width='300' height='120' /></td></tr></tbody></table><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p>";
            string PlantillaP="";
            byte[] documentoByte = null;
            List<string> sepacion = new List<string>();

            sepacion = PlantillaF.Split("dddd").ToList();

            //using (MemoryStream ms = new MemoryStream())
            //{
            //    Bitmap bitmap = new Bitmap(1200, 1800);
            //    Graphics g = Graphics.FromImage(bitmap);
            //    TheArtOfDev.HtmlRenderer.PdfSharp.HtmlContainer c = new TheArtOfDev.HtmlRenderer.PdfSharp.HtmlContainer();
            //    c.SetHtml("<html><body style='font-size:20px'>Whatever</body></html>");
            //    c.PerformPaint(g);
            //    PdfSharp.Pdf.PdfDocument doc = new PdfSharp.Pdf.PdfDocument();
            //    PdfSharp.Pdf.PdfPage page = new PdfSharp.Pdf.PdfPage();
            //    XImage img = XImage.FromGdiPlusImage(bitmap);
            //    doc.Pages.Add(page);
            //    XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
            //    xgr.DrawImage(img, 0, 0);
            //    doc.Save(@"C:\test.pdf");
            //    doc.Close();
            //}

            using (MemoryStream ms = new MemoryStream())
            {
                FontFactory.RegisterDirectories();
                using (Document _document = new Document(PageSize.A4.Rotate(), 89, 89, 143, 75))
                {
                    FontFactory.GetFont("Times New Roman", 14, iTextSharp.text.BaseColor.BLACK);
                    
                    HTMLWorker htmlparser = new HTMLWorker(_document);
                    htmlparser.SetStyleSheet(GenerateStyleSheet2());
                    PdfWriter.GetInstance(_document, ms);
                    _document.Open();
                    int Conteo = 0;
                    int cantidadItems = 0;
                    List<string> estructura = new List<string>();

                    string pathfuente = System.AppDomain.CurrentDomain.BaseDirectory + "IMPRISHA.TTF";

                    BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
                    iTextSharp.text.Font fuente = new iTextSharp.text.Font(bf, 10, 0, BaseColor.BLACK);

                    foreach (var item in sepacion)
                    { 
                        if(item.Contains("CERTIFICADO"))
                        {
                            _document.Add(new iTextSharp.text.Paragraph("CERTIFICADO", fuente));
                        }   
                        htmlparser.Parse(new StringReader(item));
                    }
                    //htmlparser.Parse(new StringReader(PlantillaF));
                    //var a = new Paragraph("aaaaaaaaaaaaaaaaa");
                    //a.Alignment = Element.ALIGN_CENTER;
                    //_document.Add(a) ;
                    //htmlparser.Parse(new StringReader(PlantillaF));
                    //_document.Add(new Paragraph("aaaaaaaaaaaaaaaaa"));

                    _document.Close();
                }
                documentoByte = ms.ToArray();
            }


            var ms1 = new MemoryStream();
            ms1.Write(documentoByte, 0, documentoByte.Length);
            ms1.Position = 0;
            return new FileStreamResult(ms1, "application/pdf");
        }
        [Route("[action]")]
        [HttpGet]
        public ActionResult GenerarPDF5()
        {
            byte[] documentoByte = null;
            System.Drawing.Image image  = null;
            using (MemoryStream ms = new MemoryStream())
            {
                string PlantillaF = $@"<span></span><p style='text-align:center;'>&nbsp;</p><table><tbody><tr style='height:100%;'><td style='width:33.333333333333336%;'>&nbsp;</td><td style='width:33.3333%;text-align:center;'><img src='https://virtual.bsginstitute.com/logo_pc2.png' alt='Logo' width='50' height='50' style='text-align:center;' /></td><td style='width:33.333333333333336%;'>&nbsp;</td></tr></tbody></table><p style='text-align:center;'>&nbsp;</p><h1 style='text-align:center;'>&lt;dddddd&gt;<span style='font-family:'Times New Roman';'>&nbsp;</span><span style='font-family:'Times New Roman';'>CERTIFICADO</span><span style='font-family:'Times New Roman';'>&nbsp;</span>&lt;/dddddd&gt;<span style='font-family:'Times New Roman';text-align:center;'></span></h1><p style='text-align:left;'>&lt;dddddd&gt; <br />Se otorga el siguiente certificado a:</p><p style='text-align:center;'><span>FISCHER&nbsp;JUVENAL&nbsp;VALDEZ&nbsp;HERRERA</span></p><p style='text-align:center;'>&nbsp;</p><p style='text-align:left;'>Por haber completado en forma satisfactoria el Programa Internacional:</p><p style='text-align:center;'>Curso Gestión de Proyectos</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>Desarrollado desde el 05 de abril del 2019 al 24 de julio del 2019,&nbsp; con una duraci&oacute;n de 4 meses horas cronol&oacute;gicas,</p><p style='text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario <span style='font-size:7px;'>(detallado al reverso)</span>.</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:right;'>Lima, Mayo del 2020</p><p style='text-align:right;'>&lt;/dddddd&gt;&nbsp;</p><table><tbody><tr style='height:100%;'><td style='width:50%;'>&nbsp;</td><td style='width:50%;'><img src='http://integra.bsgrupo.net:900/Content/img/Coordinador/Certificado/certificado_firma_jimmy.png' alt='' width='300' height='120' /></td></tr></tbody></table><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p><p style='text-align:center;'>&nbsp;</p>";
                image = TheArtOfDev.HtmlRenderer.WinForms.HtmlRender.RenderToImage(PlantillaF);
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                
                documentoByte = ms.ToArray();

                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                return File(ms.ToArray(), "image/png");
            }
        }
        [Route("[action]")]
        [HttpGet]
        public ActionResult GenerarPDF6()
        {
            byte[] documentoByte = null;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            //var privateFontCollection = new PdfSharp.Drawing.XPrivateFontCollection();
            //HttpContext.Current.Request.MapPath

            QRCodeGenerator qRCodeGenerator = new QRCodeGenerator();
            QRCodeData qrDatos = qRCodeGenerator.CreateQrCode("www.facebook.com", QRCodeGenerator.ECCLevel.H);
            QRCode qRCodigo = new QRCode(qrDatos);

            Bitmap qrImagen = qRCodigo.GetGraphic(1, Color.Black, Color.White, true);
            System.Drawing.Image objImage = (System.Drawing.Image)qrImagen;
            var a = System.IO.File.Exists(System.AppDomain.CurrentDomain.BaseDirectory);
            qrImagen.Save(System.AppDomain.CurrentDomain.BaseDirectory + "codigo.png", System.Drawing.Imaging.ImageFormat.Png);

            using (MemoryStream ms = new MemoryStream())
            {
                PdfSharp.Pdf.PdfDocument pdf = new PdfSharp.Pdf.PdfDocument();
                string PlantillaF = $@"<table style='height:375px;border:1px solid'>
   <tbody>
      <tr style='height:100%;border:1px solid'>
         <td style='width:100%;border:1px solid;vertical-align:top;'>
            <span style='font-size:17.5px;'>Otorgado a:&nbsp;<br />
               Se otorga el siguiente certificado a:
            </span>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>FISCHER&nbsp;JUVENAL&nbsp;VALDEZ&nbsp;HERRERA&nbsp;<br />
               Por haber completado en forma satisfactoria el Programa Internacional:
            </p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>Curso Gesti&oacute;n de Proyectos</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>&nbsp;</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>Desarrollado desde el 05 de abril del 2019 al 24 de julio del 2019,&nbsp; con una duraci&oacute;n de 4 meses horas cronol&oacute;gicas,</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario&nbsp;<span style='font-size:7px;'>(detallado al reverso)</span>.</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario&nbsp;<span style='font-size:7px;'>(detallado al reverso)</span>.</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario&nbsp;<span style='font-size:7px;'>(detallado al reverso)</span>.</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario&nbsp;<span style='font-size:7px;'>(detallado al reverso)</span>.</p>
            <p style='margin-bottom:0px;padding:0px;text-align:center;'>obteniendo una calificaci&oacute;n promedio de 85.29/100.00, en el cual se desarroll&oacute; el siguiente temario&nbsp;<span style='font-size:7px;'>(detallado al reverso)</span>.</p>
         </td>
      </tr>
   </tbody>
</table>
<table width='100%;border:1px solid'>
   <tbody>
      <tr style='border:1px solid'>
         <td style='border:1px solid'>
            <p style='text-align:right;font-size:16.5px;'><span style='font-family:'Times New Roman';'>Lima, Mayo del 2020</span></p>
         </td>
      </tr>
   </tbody>
</table>";


                string plantillaP = $@"<table style='height:435px;vertical-align: text-top;'>
                                           <tbody style='vertical-align: text-top;'>
                                              <tr style='height:100%;vertical-align: text-top;'>
                                                 <td style='width:100%;vertical-align: text-top;'>
                                                    <p style='margin-bottom:0px;padding:0px;'><span style='font-family:'Times New Roman';font-size:18px;color:#808080;'>Contenido del:</span></p><br>
                                                    <span style='margin-bottom:0px;padding:0px;'><span style='font-family:'Arial';font-size:22px;color:#575656;'><STRONG>PROGRAMA INTERNACIONAL EN GERENCIA DE PROYECTOS</STRONG></span></span>
                                                    
                                                    <p style='font-family:'Arial';font-size:16px;color:#575656;'>
                                                    <table style='margin-left: -30px;margin-top:-20px;vertical-align:text-top;border:1px solid;padding-top:0px'><tr style='vertical-align:text-top;border:1px solid'><td style='vertical-align:text-top;text-align:left;font-weight: normal;border:1px solid;'> 
                                                    <ul style='margin-top:-20px'>
                                                        <li style='padding-bottom:5px'><span>Introducci&oacute;n a la direcci&oacute;n de proyectos bajo los enfoques del PMI: Enfoque tradicional y Agile (4 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n de la Integraci&oacute;n (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Habilidades Directivas para la Gesti&oacute;n de Proyectos (8 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n del Alcance, Cronograma y Costos (24 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n de la Calidad (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n de los Interesados y Gesti&oacute;n de las Comunicaciones del Proyecto (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n de los Riesgos (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Gesti&oacute;n de los Recursos y las Adquisiciones del Proyecto (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>MS Project para la Gesti&oacute;n de Proyectos (12 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Enfoque &Aacute;gile en Direcci&oacute;n de Proyectos (8 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Taller de Documentaci&oacute;n Integrada de Proyecto, monitoreo y control de proyecto (8 horas cronol&oacute;gicas)</span></li>
                                                        <li style='padding-bottom:5px'><span>Preparaci&oacute;n para el examen PMP<em><suli>&reg;</suli> </em>(36 horas cronol&oacute;gicas) (1)</span></li>
                                                    </td></tr></table></p>
                                                    
                                                 </td>
                                              </tr>
                                           </tbody>
                                        </table>
                                        <table style='vertical-align:text-top;border:1px solid'>
                                           <tbody style='vertical-align:text-top;border:1px solid'>
                                              <tr style='height:100%;vertical-align:text-top;border:1px solid'>
                                                 <td style='width:100%;border:1px solid'><span style='color:#808080;font-family:Arial;'>Lima, Enero del 2020000</span></td>
                                              </tr>
                                           </tbody>
                                        </table>";
                string complemento = @"
                                       <head>
                                        <style>
                                            td { page-break-inside: avoid; }
                                            table { page-break-inside: avoid; }
                                        </style>
                                  </head>
                                      ";
                string tabla = complemento+ $@"
                <table  style='font-size:11px; font-family:Arial ;border:1px solid black;border-collapse: collapse;width:100%;' cellspacing='3' cellpadding='3'>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                    <th style='border:1px solid black;border-collapse: collapse;'> CURSO </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> NOTA </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> ESTADO </th>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gerencia de Sistemas de Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;text-align:center'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Planeamiento y Programación del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de RRHH en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 16</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de la Logística del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 17</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de Costos en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 0</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> DESAPROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Mantenimiento y Sistemas de Gestión 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                </table>
                <table  style='font-size:11px; font-family:Arial ;border:1px solid black;border-collapse: collapse;width:100%;' cellspacing='3' cellpadding='3'>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                    <th style='border:1px solid black;border-collapse: collapse;'> CURSO </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> NOTA </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> ESTADO </th>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gerencia de Sistemas de Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;text-align:center'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Planeamiento y Programación del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de RRHH en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 16</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de la Logística del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 17</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de Costos en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 0</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> DESAPROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Mantenimiento y Sistemas de Gestión 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                </table>
                <table  style='font-size:11px; font-family:Arial ;border:1px solid black;border-collapse: collapse;width:100%;' cellspacing='3' cellpadding='3'>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                    <th style='border:1px solid black;border-collapse: collapse;'> CURSO </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> NOTA </th>
                    <th style='border:1px solid black;border-collapse: collapse;'> ESTADO </th>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gerencia de Sistemas de Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;text-align:center'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Planeamiento y Programación del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de RRHH en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 16</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de la Logística del Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 17</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Gestión de Costos en Mantenimiento 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 0</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> DESAPROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Mantenimiento y Sistemas de Gestión 2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 18</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                    <tr style='border:1px solid black;border-collapse: collapse;'>
                       <td style='vertical-align:top;text-align:left;border:1px solid black;border-collapse: collapse;'> Curso Medición, Control Estadístico y Mejora Continua del Mantenimiento  2019 II LIMA</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> 19</td>
                       <td style='vertical-align:top;border:1px solid black;border-collapse: collapse;'> APROBADO</td>
                    </tr>
                </table>
                ";

                //XGraphics gfx = XGraphics.FromPdfPage(page);
                //DrawImage(gfx, imageLoc, 50, 50, 250, 250);

                //var config = new PdfGenerateConfig();
                //config.PageOrientation = PdfSharp.PageOrientation.Landscape;
                //config.PageSize = PdfSharp.PageSize.A4;
                //config.MarginTop = 144;
                //config.MarginBottom = 30;
                //config.MarginLeft = 111;
                //config.MarginRight = 50;

                //var config = new PdfGenerateConfig();
                //config.PageOrientation = PdfSharp.PageOrientation.Landscape;
                //config.PageSize = PdfSharp.PageSize.A4;
                //config.MarginLeft = 257;
                //config.MarginRight = 127;
                //config.MarginTop = 49;
                //config.MarginBottom = 97;

                var config = new PdfGenerateConfig();
                config.PageSize = PdfSharp.PageSize.A4;
                config.MarginLeft = 60;
                config.MarginRight = 60;
                config.MarginTop = 60;
                config.MarginBottom = 60;


                var pdfFrontal = PdfGenerator.GeneratePdf(tabla, config);
                var temmporal = ImportarPdfDocument(pdfFrontal);


                foreach (var itempage in temmporal.Pages)
                {
                    PdfSharp.Pdf.PdfPage page = itempage;
                    pdf.AddPage(page);
                }
                
                PdfSharp.Drawing.XGraphics gfx = PdfSharp.Drawing.XGraphics.FromPdfPage(pdf.Pages[0], PdfSharp.Drawing.XGraphicsPdfPageOptions.Prepend);

                //HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/CERTIFICADO-FONFO-02.png");
                //HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/CERTIFICADO-FONFO-REVERSO-04.png");
                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/hoja-memb-11.png");
                webRequest.AllowWriteStreamBuffering = true;
                WebResponse webResponse = webRequest.GetResponse();
                PdfSharp.Drawing.XImage xImage = PdfSharp.Drawing.XImage.FromStream(webResponse.GetResponseStream());
                gfx.DrawImage(xImage, 0, 0, 595, 843);

                PdfSharp.Drawing.XGraphics gfx2 = PdfSharp.Drawing.XGraphics.FromPdfPage(pdf.Pages[1], PdfSharp.Drawing.XGraphicsPdfPageOptions.Prepend);

                //HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/CERTIFICADO-FONFO-02.png");
                //HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/CERTIFICADO-FONFO-REVERSO-04.png");
                HttpWebRequest webRequest2 = (HttpWebRequest)HttpWebRequest.Create("https://repositorioweb.blob.core.windows.net/repositorioweb/certificados/hoja-memb-11.png");
                webRequest2.AllowWriteStreamBuffering = true;
                WebResponse webResponse2 = webRequest2.GetResponse();
                PdfSharp.Drawing.XImage xImage2 = PdfSharp.Drawing.XImage.FromStream(webResponse2.GetResponseStream());
                gfx2.DrawImage(xImage2, 0, 0, 595, 843);

                //PdfSharp.Drawing.XGraphics gfx = PdfSharp.Drawing.XGraphics.FromPdfPage(page, PdfSharp.Drawing.XPageDirection.Downwards);

                //gfx.DrawImage(PdfSharp.Drawing.XImage.FromFile("c:\\codigo.jpeg"), 0, 0);


                //PdfSharp.Pdf.PdfDocument pdf = PdfGenerator.GeneratePdf(PlantillaF, config);

                pdf.Save(ms,false);
                documentoByte = ms.ToArray();
            }
            var ms1 = new MemoryStream();
            ms1.Write(documentoByte, 0, documentoByte.Length);
            ms1.Position = 0;
            return new FileStreamResult(ms1, "application/pdf");
        }
        void DrawImage(XGraphics gfx, string jpegSamplePath, int x, int y, int width, int height)
        {
            XImage image = XImage.FromFile(jpegSamplePath);
            gfx.DrawImage(image, x, y, width, height);
        }
     
        public PdfSharp.Pdf.PdfDocument ImportarPdfDocument(PdfSharp.Pdf.PdfDocument pdf1)
        {
            using (var stream = new MemoryStream())
            {
                pdf1.Save(stream, false);
                stream.Position = 0;
                var result = PdfSharp.Pdf.IO.PdfReader.Open(stream, PdfSharp.Pdf.IO.PdfDocumentOpenMode.Import);
                return result;
            }
        }
        private StyleSheet GenerateStyleSheet2()
        {
            FontFactory.RegisterDirectories();

            StyleSheet css = new StyleSheet();

            css.LoadTagStyle("h1", "size", "10pt");
            css.LoadTagStyle("h1", "style", "text-align:cen1ter;font-weight:bold;");
            css.LoadTagStyle("h2", "face", "times new roman");
            css.LoadTagStyle("p", "style", "text-align:justify;");
            css.LoadTagStyle("div", "size", "10pt");
            css.LoadTagStyle("ul", "style", "display:block;list-style-type:circle;");
            //css.LoadTagStyle(HtmlTags.TABLE, HtmlTags.BORDER, "0.1");
            css.LoadTagStyle(HtmlTags.DIV, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.H1, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.H2, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.H2, HtmlTags.FONTSIZE, "10px");
            css.LoadTagStyle(HtmlTags.H3, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.H3, HtmlTags.FONTSIZE, "10px");
            css.LoadTagStyle(HtmlTags.H4, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.H4, HtmlTags.FONTSIZE, "10px");
            css.LoadTagStyle(HtmlTags.P, HtmlTags.FONTFAMILY, "Times New Roman");
            css.LoadTagStyle(HtmlTags.P, HtmlTags.FONTSIZE, "25px");
            css.LoadTagStyle(HtmlTags.SPAN, HtmlTags.FONTFAMILY, "Times New Roman");
            return css;
        }

        
    }
}
