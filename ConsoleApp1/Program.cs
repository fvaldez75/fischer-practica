﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int posicion = int.Parse(Console.ReadLine());
            int contador = 0;
            int actual = 0;
            int anterior = 1;
            int aux = 0;
            while (contador < posicion)
            {
                aux = anterior;
                anterior = actual;
                actual = actual + aux;
                contador++;
            }
             Console.WriteLine(actual);
        }
    }
}
